// function readBooks(time, book, callback) {
//     console.log(`saya membaca ${book.name}`)
//     setTimeout(function () {
//         let sisaWaktu = 0
//         if (time > book.timeSpent) {
//             sisaWaktu = time - book.timeSpent
//             console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
//             callback(sisaWaktu) //menjalankan function callback
//         } else {
//             console.log('waktu saya habis')
//             callback(time)
//         }
//     }, book.timeSpent)
// }

// module.exports = readBooks 


// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

var i = 0;

// Recursive
function book(time, books) {
    readBooks(time, books[i], function (get) {
        i++;
        if (books[i] != undefined) {
            book(get, books);
        }
    });
}
book(10000, books);
